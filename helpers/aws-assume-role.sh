#!/bin/bash
# Helper to improve aws assume role process, to use enter the "Variables" values and run the bash script.
# Tips: Add this script on bash alias:
# mkdir ~/bash_aliases/
# curl -o ~/bash_aliases/aws-assume-role.sh https://gitlab.com/ganex-cloud/gitlab-ci-templates/-/raw/main/helpers/aws-assume-role.sh
# echo 'alias assume-role="source ~/bash_aliases/aws-assume-role.sh"' >> ~/.bash_aliases

# Variables
AWS_MFA_TOKEN_ARN=""
AWS_ROLE_ARN="arn:aws:iam::${AWS_ACCOUNT}:role/XXX"
AWS_ROLE_SESSION_NAME="username-terraform"
DURATION_SECONDS="3600"

# Script 
if [[ -f "main.tf" ]]; then
  AWS_ACCOUNT=`cat main.tf | grep allowed_account_ids | awk '{print $3}' | sed 's/"//g' | sed 's/\[//g' | sed 's/\]//g'`
else
  read -p "AWS Account id: " AWS_ACCOUNT
fi
AWS_ROLE_ARN="arn:aws:iam::${AWS_ACCOUNT}:role/ganex"

read -p "Input MFA Code: " AWS_MFA_TOKEN
if [ "$AWS_MFA_TOKEN" = "" ]; then
  MFA_PARAMETERS=""
else
  MFA_PARAMETERS="--serial-number ${AWS_MFA_TOKEN_ARN} --token-code ${AWS_MFA_TOKEN}"
fi

# Assume role
OUTPUT=`aws sts assume-role --role-arn "${AWS_ROLE_ARN}" --role-session-name "${AWS_ROLE_SESSION_NAME}" --duration-seconds 3600 ${MFA_PARAMETERS}`

# Export AWS Keys
if [ $? -gt 0 ]; then
  echo 
  echo "Error assuming role \"${AWS_ROLE_ARN}\" on account \"${AWS_ACCOUNT}\""
else
  export AWS_ACCESS_KEY_ID=`echo $OUTPUT | jq -r .Credentials.AccessKeyId`
  export AWS_SECRET_ACCESS_KEY=`echo $OUTPUT | jq -r .Credentials.SecretAccessKey`
  export AWS_SESSION_TOKEN=`echo $OUTPUT | jq -r .Credentials.SessionToken`
  echo 
  echo "Role \"${AWS_ROLE_ARN}\" on account \"${AWS_ACCOUNT}\" successfully assumed!"
fi